require 'rho/rhocontroller'
require 'helpers/browser_helper'
require 'helpers/advance_search'
require 'helpers/near_by'
require "date"
require 'json'

class UserController < Rho::RhoController
  include BrowserHelper
  include AdvanceSearch
  include NearBy

  # GET /User
  def index
    @users = User.find(:all)
    render :back => '/app'
  end

  
  # GET /User/{1}
  def show
    @user = User.find(@params['id'])
    if @user
      render :action => :show, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # GET /User/new
  def new
    @user = User.new
    render :action => :new, :back => url_for(:action => :index)
  end

  # GET /User/{1}/edit
  def edit
    @user = User.find(@params['id'])
    if @user.blank?
      render :action => :edit, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # POST /User/create
  def create
    @user = User.create(@params['user'])
    redirect :action => :index
  end

  # POST /User/{1}/update
  def update
    @user = User.find(@params['id'])
    @user.update_attributes(@params['user']) if @user
    redirect :action => :index
  end

  # POST /User/{1}/delete
  def delete
    @user = User.find(@params['id'])
    @user.destroy if @user
    redirect :action => :index
  end

end
