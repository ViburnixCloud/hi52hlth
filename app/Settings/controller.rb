require 'rho'
require 'rho/rhocontroller'
require 'rho/rhoerror'
require 'helpers/browser_helper'

class SettingsController < Rho::RhoController
  include BrowserHelper
  
  def index
    @msg = @params['msg']
    render
  end

  def home 
    jsonContent = Rho::JSON.parse('[{"countryname":"","home_city":"","lastname":"Amuthaa","image_url":"/images/no_image_thumb.jpg","is_profile_address":"0","business_address":"","email":"","firstname":"A","mobile_number":"999999999","about_me":null,"batch_in":"2005","fullname":"A  Amuthaa","user_id":"6853","course":"MDP","is_profile_email":"0","batch_passout":"2005","member_type":1,"middlename":"","linkedin_profile":null,"home_address":""},{"countryname":"","home_city":"","lastname":"ARORA","image_url":"/images/no_image_thumb.jpg","is_profile_address":"0","business_address":"","email":"","firstname":"A","mobile_number":null,"about_me":null,"batch_in":"1986","fullname":"A  ARORA","user_id":"7518","course":"PGP","is_profile_email":"0","batch_passout":"1988","member_type":1,"middlename":"","linkedin_profile":null,"home_address":""},{"countryname":"","home_city":"","lastname":"BHARDWAJ","image_url":"/images/no_image_thumb.jpg","is_profile_address":"0","business_address":"","email":"","firstname":"A","mobile_number":"999999999","about_me":null,"batch_in":"1994","fullname":"A  BHARDWAJ","user_id":"8120","course":"MDP","is_profile_email":"0","batch_passout":"1994","member_type":1,"middlename":"","linkedin_profile":null,"home_address":""},{"countryname":"","home_city":"","lastname":"BHATTACHARJEE","image_url":"/images/no_image_thumb.jpg","is_profile_address":"0","business_address":"","email":"","firstname":"A","mobile_number":null,"about_me":null,"batch_in":"1977","fullname":"A  BHATTACHARJEE","user_id":"8181","course":"PGP","is_profile_email":"0","batch_passout":"1979","member_type":1,"middlename":"","linkedin_profile":null,"home_address":""},{"countryname":"","home_city":"","lastname":"BHATTACHARYA","image_url":"/images/no_image_thumb.jpg","is_profile_address":"0","business_address":"","email":"","firstname":"A","mobile_number":null,"about_me":null,"batch_in":"1975","fullname":"A  BHATTACHARYA","user_id":"8151","course":"PGP","is_profile_email":"0","batch_passout":null,"member_type":1,"middlename":"","linkedin_profile":null,"home_address":""},{"countryname":"","home_city":"","lastname":"BHATTACHARYYA","image_url":"/images/no_image_thumb.jpg","is_profile_address":"0","business_address":"","email":"","firstname":"A","mobile_number":"999999999","about_me":null,"batch_in":"2000","fullname":"A  BHATTACHARYYA","user_id":"6842","course":"MDP","is_profile_email":"0","batch_passout":"2000","member_type":1,"middlename":"","linkedin_profile":null,"home_address":""},{"countryname":"","home_city":"","lastname":"CHAKRABORTY","image_url":"/images/no_image_thumb.jpg","is_profile_address":"0","business_address":"","email":"","firstname":"A","mobile_number":"999999999","about_me":null,"batch_in":"2000","fullname":"A  CHAKRABORTY","user_id":"6818","course":"MDP","is_profile_email":"0","batch_passout":"2000","member_type":1,"middlename":"","linkedin_profile":null,"home_address":""},{"countryname":"","home_city":"","lastname":"CHANDRASEKARAN","image_url":"/images/no_image_thumb.jpg","is_profile_address":"0","business_address":"","email":"","firstname":"A","mobile_number":"999999999","about_me":null,"batch_in":"2000","fullname":"A  CHANDRASEKARAN","user_id":"6825","course":"MDP","is_profile_email":"0","batch_passout":"2000","member_type":1,"middlename":"","linkedin_profile":null,"home_address":""},{"countryname":"","home_city":"","lastname":"Geetha","image_url":"/images/no_image_thumb.jpg","is_profile_address":"0","business_address":"","email":"","firstname":"A","mobile_number":null,"about_me":null,"batch_in":"2005","fullname":"A  Geetha","user_id":"6826","course":"MDP","is_profile_email":"0","batch_passout":"2005","member_type":1,"middlename":"","linkedin_profile":null,"home_address":""},{"countryname":"","home_city":"","lastname":"GUHA","image_url":"/images/no_image_thumb.jpg","is_profile_address":"0","business_address":"","email":"","firstname":"A","mobile_number":null,"about_me":null,"batch_in":"1982","fullname":"A  GUHA","user_id":"6599","course":"PGP","is_profile_email":"0","batch_passout":"1984","member_type":1,"middlename":"","linkedin_profile":null,"home_address":""}]')
    jsonContent.each do |user|
      User.create(user)
    end
    redirect(:controller => 'User', :action => :home)
  end
  def reset
    render :action => :reset
  end
  
  def do_reset
    Rhom::Rhom.database_full_reset
    Rho::RhoConnectClient.doSync
    @msg = "Database has been reset."
    redirect :action => :index, :query => {:msg => @msg}
  end
  
  def do_sync
    Rho::RhoConnectClient.doSync
    @msg =  "Sync has been triggered."
    redirect :action => :index, :query => {:msg => @msg}
  end

end
