# The model has already been created by the framework, and extends Rhom::RhomObject
# You can add more methods here
class WidgetType
  include Rhom::PropertyBag

  # Uncomment the following line to enable sync with WidgetType.
  # enable :sync

  #add model specific code here
  
  def self.listing(widget)
    conditions = widget[:conditions] || {}
#    class_name = Object.const_set(widget[:model].split('_').collect(&:capitalize).join,Class.new)
    data = Object.const_get(widget[:model].split('_').collect(&:capitalize).join).find(:all)
   {:data => {'resources' => data}, :layout => widget[:layout]}
  end
end