require 'rho/rhocontroller'
require 'helpers/browser_helper'

class WidgetTypeController < Rho::RhoController
  include BrowserHelper

  # GET /WidgetType
  def index
    @widgettypes = WidgetType.find(:all)
    render :back => '/app'
  end

  # GET /WidgetType/{1}
  def show
    @widgettype = WidgetType.find(@params['id'])
    if @widgettype
      render :action => :show, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # GET /WidgetType/new
  def new
    @widgettype = WidgetType.new
    render :action => :new, :back => url_for(:action => :index)
  end

  # GET /WidgetType/{1}/edit
  def edit
    @widgettype = WidgetType.find(@params['id'])
    if @widgettype
      render :action => :edit, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # POST /WidgetType/create
  def create
    @widgettype = WidgetType.create(@params['widgettype'])
    redirect :action => :index
  end

  # POST /WidgetType/{1}/update
  def update
    @widgettype = WidgetType.find(@params['id'])
    @widgettype.update_attributes(@params['widgettype']) if @widgettype
    redirect :action => :index
  end

  # POST /WidgetType/{1}/delete
  def delete
    @widgettype = WidgetType.find(@params['id'])
    @widgettype.destroy if @widgettype
    redirect :action => :index  
  end
end
